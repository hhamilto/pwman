
const storageOnChangedDropboxSyncHandler = async (changes, areaName) => {
	if (areaName != 'local') {
		return
	}
	if (!changes.vaultChangedCanaryKey) {
		return
	}
	await pwman.helpers.syncToDropbox()
}

browser.commands.onCommand.addListener((command) => {
	if (command === "request-fill") {
		return true
	}
})

;(async () => {
	await pwman.helpers.loadVaultFromLocalStorage()
	if (browser.storage.onChanged.hasListener(storageOnChangedDropboxSyncHandler)) {
		return
	}
	browser.storage.onChanged.addListener(storageOnChangedDropboxSyncHandler)
})()


browser.runtime.onMessage.addListener(async (message) => {
	if (message.action == 'request fill') {
		await pwman.helpers.loadVaultFromLocalStorage()
		const items = await window.pwman.helpers.searchItems({
			origin: message.origin
		})
		return items
	} else if (message.action == 'dropbox-auth') {
		await pwman.helpers.doDropboxOAuth()
		browser.storage.onChanged.addListener(storageOnChangedDropboxSyncHandler)
		await pwman.helpers.syncToDropbox()
		return
	} else if (message.action == 'dropbox-import') {
		await pwman.helpers.doDropboxOAuth()
		await pwman.helpers.importFromDropbox()
		browser.storage.onChanged.addListener(storageOnChangedDropboxSyncHandler)
		return
	}
	pwman.log('unrecognized message recv by background script:', message)
})
