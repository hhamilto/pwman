
// FIXME: Remove google bullshit
pwman = {}
pwman.screens = {}
pwman.credentials = {
	token: null,
	tokenExpiration: null,
}

pwman.log = (...args) => {
	// eslint-disable-next-line no-console
	console.log(...args)
}

pwman.constants = {
	SERVER_BASE_URI: "https://pwman.cordshamilton.com"
}

const IN_BROWSER_TAB = false
// mock browser for testing in browser
if (IN_BROWSER_TAB) {
	const IS_LOGGED_IN = true
	browser = {}
	browser.storage = {}
	browser.storage.local = {}
	browser.storage.local.get = async () => {
		// todo -- check what is being gotten if we use localstorage for other stuff
		if (IS_LOGGED_IN) {
			return {
				token: 'qux',
				tokenExpiration: '2222-01-01'
			}
		}
		return {}
	}
	browser.storage.local.set = async () => {}
	browser.tabs = {}
	browser.tabs.query = async () => {
		return [{
			id: 'baz'
		}]
	}
	browser.tabs.sendMessage = async () => {
		return 'http://localhost'
	}
}

(() => {
	const BACKUP_FILENAME = 'pwman-backup.json'
	const REDIRECT_URL = browser.identity.getRedirectURL()
	const DROPBOX_CLIENT_ID = "oup1eb0gdm5dfst"

	// gloabl helper object
	pwman.helpers = {
		guessItemFromPage: async () => {
			const [currentTab] = await browser.tabs.query({
				currentWindow: true,
				active: true
			})
			const url = await browser.tabs.sendMessage(
				currentTab.id,
				{
					action: "fetch url"
				}
			)
			const parsedURL = new URL(url)
			const {origin} = parsedURL
			document.querySelector('#add-item .website').value = origin

			const username = await browser.tabs.sendMessage(
				currentTab.id,
				{
					action: "fetch username"
				}
			)
			if (username) {
				document.querySelector('#add-item .username').value = username
			}

			const password = await browser.tabs.sendMessage(
				currentTab.id,
				{
					action: "fetch password"
				}
			)
			if (password) {
				document.querySelector('#add-item .password').value = password
			}
		},
		ab2str: (buf) => {
			return String.fromCharCode.apply(null, new Uint16Array(buf))
		},
		str2ab: (str) => {
			// 2 bytes for each char
			var buf = new ArrayBuffer(str.length * 2)
			var bufView = new Uint16Array(buf)
			for (var i = 0, strLen = str.length; i < strLen; i++) {
				bufView[i] = str.charCodeAt(i)
			}
			return buf
		},
		searchItemsForCurrentPage: async () => {
			if (IN_BROWSER_TAB) {
				// send mock items
				pwman.log("WARNING USING MOCK ITEMS")
				return [{
					item: {
						username: 'hello',
						password: 'testpw',
						website: 'http://localhost:8080'
					},
					id: 'foo'
				}]
			}
			const [currentTab] = await browser.tabs.query({
				currentWindow: true,
				active: true
			})
			const url = await browser.tabs.sendMessage(
				currentTab.id,
				{
					action: "fetch url"
				}
			)
			const parsedURL = new URL(url)
			const {origin} = parsedURL
			return pwman.helpers.searchItems({origin})
		},
		searchItems: async ({origin, fuzzy = false}) => {
			let regexp
			if (fuzzy) {
				regexp = new RegExp('^.*' + origin + '.*$')
			} else {
				regexp = new RegExp('^' + origin + '$')
			}

			const matchingItems = []
			for (let i = 0; i < pwman.vault.length; i++) {
				const item = pwman.vault[i].item
				for (let j = 0; j < item.website.length; j++) {
					if (regexp.test(item.website[j])) {
						matchingItems.push(pwman.vault[i])
						break
					}
				}
			}
			return matchingItems
		},
		debounce: (timeoutMs, fun) => {
			let timeoutHandle
			return (...args) => {
				if (timeoutHandle) {
					clearTimeout(timeoutHandle)
				}
				timeoutHandle = setTimeout(() => {
					timeoutHandle = null
					fun(...args)
				}, timeoutMs)
			}
		},
		typedArray2hex: (array) => {
			return array.
			reduce((hex, x) => hex + x.toString(16).padStart(2, '0'), '').
			toUpperCase()
		},
		hex2Uint8Array: (hexStr) => {
			const HEX_CHAR_BYTE_LENGTH = 2
			if (hexStr.length % HEX_CHAR_BYTE_LENGTH != 0) {
				throw new Error("invalid hex string")
			}
			const array = new Uint8Array(hexStr.length / 2)
			for (let i = 0; i < hexStr.length; i += HEX_CHAR_BYTE_LENGTH) {
				array[i / HEX_CHAR_BYTE_LENGTH] = parseInt(hexStr.substr(i, HEX_CHAR_BYTE_LENGTH), 16)
			}
			return array
		},
		deriveMasterKey: async ({password, vaultKey, salt}) => {
			// Note: Hash master password using PBKDF2 to obtain AES key for vault
			const encoder = new TextEncoder()
			const passwordKey = await window.crypto.subtle.importKey(
				'raw',
				encoder.encode(password + vaultKey),
				'PBKDF2',
				true,
				['deriveKey']
			)

			const deriveKeyAlgoParams = {
				name: 'PBKDF2',
				hash: 'SHA-256',
				salt: salt,
				iterations: 100000
			}
			const aesKeyParams = {
				name: 'AES-GCM',
				length: 256,
			}
			const masterKey = await window.crypto.subtle.deriveKey(
				deriveKeyAlgoParams,
				passwordKey,
				aesKeyParams,
				true,
				['encrypt', 'decrypt']
			)
			return masterKey
		},
		syncVaultToLocalStorage: async () => {
			const NUM_IV_BITS = 96
			const iv = new Uint8Array(NUM_IV_BITS / 8)
			window.crypto.getRandomValues(iv)
			const encryptedVault = await window.crypto.subtle.encrypt({
				// XXX Q Is this using 256 bit AES-GCM
				name: 'AES-GCM',
				iv: iv,
			}, pwman.masterKey, pwman.helpers.str2ab(JSON.stringify(pwman.vault)))
			await browser.storage.local.set({
				iv,
				vault: encryptedVault,
			})
			await browser.storage.local.set({
				// Note: change event handlers not firing for vault changes, so use this key to trigger
				vaultChangedCanaryKey: new Date().toString()
			})
		},
		loadVaultFromLocalStorage: async () => {
			const storedInfo = await browser.storage.local.get(['masterKey', 'vault', 'iv'])
			if (!storedInfo.vault || !storedInfo.iv || !storedInfo.masterKey) {
				// TODO -- silent fail?
				return
			}
			pwman.masterKey = await window.crypto.subtle.importKey(
				'jwk',
				storedInfo.masterKey,
				{ name: 'AES-GCM'},
				true,
				['encrypt', 'decrypt']
			)
			pwman.vault = JSON.parse(pwman.helpers.ab2str(await window.crypto.subtle.decrypt({
				name: 'AES-GCM',
				iv: storedInfo.iv,
			}, pwman.masterKey, storedInfo.vault)))
		},
		doDropboxOAuth: async () => {
			// ref: https://dropbox.tech/developers/pkce--what-and-why-
			const dec2hex = (dec) => {
				return ("0" + dec.toString(16)).substr(-2)
			}

			const generateCodeVerifier = () => {
				const array = new Uint32Array(56 / 2)
				window.crypto.getRandomValues(array)
				return Array.from(array, dec2hex).join("")
			}

			const sha256 = (plain) => {
				// returns promise ArrayBuffer
				const encoder = new TextEncoder()
				const data = encoder.encode(plain)
				return window.crypto.subtle.digest("SHA-256", data)
			}

			const base64urlencode = (a) => {
				var str = ""
				var bytes = new Uint8Array(a)
				var len = bytes.byteLength
				for (var i = 0; i < len; i++) {
					str += String.fromCharCode(bytes[i])
				}
				return btoa(str).
					replace(/\+/g, "-").
					replace(/\//g, "_").
					replace(/[=]+$/, "")
			}

			const generateCodeChallengeFromVerifier = async (v) => {
				var hashed = await sha256(v)
				var base64encoded = base64urlencode(hashed)
				return base64encoded
			}

			const formEncode = (bodyObject) => {
				const bodyArr = []
				for (const property in bodyObject) {
					if (!bodyObject[property]) {
						continue
					}
					var encodedKey = encodeURIComponent(property)
					var encodedValue = encodeURIComponent(bodyObject[property])
					bodyArr.push(encodedKey + "=" + encodedValue)
				}
				return bodyArr.join("&")
			}

			const codeVerifier = generateCodeVerifier()
			const codeChallenge = await generateCodeChallengeFromVerifier(codeVerifier)

			const AUTH_URL = 'https://www.dropbox.com/oauth2/authorize?client_id=' + DROPBOX_CLIENT_ID
				+ '&redirect_uri=' + encodeURIComponent(REDIRECT_URL)
				+ '&code_challenge=' + encodeURIComponent(codeChallenge)
				+ '&response_type=code'
				+ '&code_challenge_method=S256'
			const redirectUriString = await browser.identity.launchWebAuthFlow({
				interactive: true,
				url: AUTH_URL
			})
			const redirectUrl = new URL(redirectUriString)
			const oauthCode = redirectUrl.searchParams.get('code')


			const tokenResp = await fetch('https://api.dropboxapi.com/oauth2/token', {
				method: "POST",
				headers: {
					"Content-type": "application/x-www-form-urlencoded",
				},
				body: formEncode({
					code: oauthCode,
					client_id: DROPBOX_CLIENT_ID,
					code_verifier: codeVerifier,
					redirect_uri: REDIRECT_URL,
					grant_type: 'authorization_code'
				})
			})
			const tokenRespJson = await tokenResp.json()
			await browser.storage.local.set({
				dropboxAccessToken: tokenRespJson.access_token
			})
		},
		// XXX (semi important incase this backup breaks) - how to propagate errors from this background function to the UI
		syncToDropbox: async (retries = 2) => {
			retries--

			const storedItems = await browser.storage.local.get(['vaultKey', 'masterKeySalt', 'vault', 'iv', 'dropboxAccessToken'])
			const backupData = {}
			backupData.vaultKey = storedItems.vaultKey
			backupData.vault = pwman.helpers.ab2str(storedItems.vault)
			backupData.iv = pwman.helpers.typedArray2hex(storedItems.iv)
			backupData.masterKeySalt = pwman.helpers.typedArray2hex(storedItems.masterKeySalt)

			const respRaw = await fetch('https://content.dropboxapi.com/2/files/upload', {
				method: "POST",
				headers: {
					Authorization: "Bearer " + storedItems.dropboxAccessToken,
					"Dropbox-API-Arg": JSON.stringify({
						autorename: false,
						mode: "overwrite",
						mute: false,
						path: `/${BACKUP_FILENAME}`,
						strict_conflict: false
					}),
					"Content-Type": "application/octet-stream"
				},
				body: JSON.stringify(backupData)
			})
			if (respRaw.status == 400 || respRaw.status == 401) {
				await pwman.helpers.doDropboxOAuth()
			}
			if (respRaw.status != 200) {
				if (retries == 0) {
					await browser.storage.local.set({
						dropboxSyncFailed: true
					})
					throw new Error(`Could not update ${BACKUP_FILENAME}`)
				}
				pwman.helpers.syncToDropbox(retries)
			}
		},
		importFromDropbox: async () => {
			// TODO?: show loading screen while import is going on
			const {dropboxAccessToken} = await browser.storage.local.get(['dropboxAccessToken'])

			const backupResp = await fetch('https://content.dropboxapi.com/2/files/download', {
				method: "POST",
				headers: {
					Authorization: "Bearer " + dropboxAccessToken,
					'Dropbox-API-Arg': `{"path":"/${BACKUP_FILENAME}"}`
				}
			})
			const backupData = await backupResp.json()
			const vaultKey = backupData.vaultKey
			const vault = pwman.helpers.str2ab(backupData.vault)
			const iv = pwman.helpers.hex2Uint8Array(backupData.iv)
			const masterKeySalt = pwman.helpers.hex2Uint8Array(backupData.masterKeySalt)
			pwman.log("setting storage to: ", {
				vaultKey,
				vault,
				iv,
				masterKeySalt
			})
			await browser.storage.local.set({
				vaultKey,
				vault,
				iv,
				masterKeySalt
			})
		},
		addItem: async ({username, password, websites, notes}) => {
			const ITEM_ID_LENGTH_BITS = 128
			const randomArray = new Uint8Array(ITEM_ID_LENGTH_BITS / 8)
			window.crypto.getRandomValues(randomArray)
			const itemId = pwman.helpers.typedArray2hex(randomArray)

			const item = {
				id: itemId,
				item: {
					username,
					password,
					website: websites,
					notes
				}
			}
			pwman.vault.push(item)
			await pwman.helpers.syncVaultToLocalStorage()
		},
		fillItemOnBrowserPage: async (item, options) => {
			const [currentTab] = await browser.tabs.query({
				currentWindow: true,
				active: true
			})
			await browser.tabs.sendMessage(
				currentTab.id,
				{
					action: "fill item",
					item,
					options
				}
			)
		},

	}
})()
