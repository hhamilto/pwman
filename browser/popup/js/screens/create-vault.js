(() => {
	pwman.screens['create-vault'] = {}
	pwman.screens['create-vault'].setup = () => {
		document.querySelector('#create-vault form').addEventListener('submit', async (e) => {
			e.preventDefault()

			/*
			 * Note: Create vault key
			 * Note: 128 bits of randomness is hard to guess
			 */
			const VAULT_KEY_BITS = 128
			const randomUInt32Array = new Uint8Array(VAULT_KEY_BITS / 8)
			window.crypto.getRandomValues(randomUInt32Array)
			const vaultKey = pwman.helpers.typedArray2hex(randomUInt32Array)

			const salt = new Uint8Array(16)
			window.crypto.getRandomValues(salt)

			const password = document.querySelector('#create-vault .password').value
			const masterKey = await pwman.helpers.deriveMasterKey({password, vaultKey, salt})
			const exportedKey = await window.crypto.subtle.exportKey('jwk', masterKey)
			await browser.storage.local.set({
				vaultKey: vaultKey,
				masterKey: exportedKey,
				masterKeySalt: salt
			})

			// Note: Encrypt an empty array
			const revivedVaultKey = await window.crypto.subtle.importKey(
				'jwk',
				exportedKey,
				{ name: 'AES-GCM'},
				true,
				['encrypt', 'decrypt']
			)

			const iv = new Uint8Array(96 / 8)
			window.crypto.getRandomValues(iv)


			const encryptedVault = await window.crypto.subtle.encrypt(
				{
					name: 'AES-GCM',
					iv: iv,
				},
				revivedVaultKey,
				pwman.helpers.str2ab(JSON.stringify([]))
			)
			await browser.storage.local.set({
				vault: encryptedVault,
				iv: iv
			})
			await pwman.showScreen('main-menu')
		})
	}

	pwman.screens['create-vault'].showError = (message) => {
		const errorMessageEl = document.querySelector('#create-vault .error')
		errorMessageEl.classList.remove('hidden')
		errorMessageEl.textContent = message
	}
})()
