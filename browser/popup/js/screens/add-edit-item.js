pwman.screens['add-item'] = {}
pwman.screens['add-item'].setup = () => {
	document.querySelector('#add-item form').addEventListener('submit', async (e) => {
		e.preventDefault()
		const username = document.querySelector('#add-item .username').value
		const password = document.querySelector('#add-item .password').value
		const website = document.querySelector('#add-item .website').value
		const notes = document.querySelector('#add-item .notes').value
		const websites = website.split(',').map(w => w.trim())
		await pwman.helpers.addItem({
			username,
			password,
			websites,
			notes
		})
		await pwman.showScreen('main-menu')
	})
	document.querySelector('#add-item form .back').addEventListener('click', async (e) => {
		e.preventDefault()
		await pwman.showScreen('main-menu')
	})
}

pwman.screens['add-item'].show = async () => {
	await pwman.helpers.guessItemFromPage()
}

pwman.screens['edit-item'] = {}
pwman.screens['edit-item'].setup = () => {
	document.querySelector('#edit-item form').addEventListener('submit', async (e) => {
		e.preventDefault()
		const username = document.querySelector('#edit-item .username').value
		const password = document.querySelector('#edit-item .password').value
		const website = document.querySelector('#edit-item .website').value
		const notes = document.querySelector('#edit-item .notes').value
		const websites = website.split(',').map(w => w.trim())
		const itemId = document.querySelector('#edit-item .item-id').value
		let item
		for (let i = 0; i < pwman.vault.length; i++) {
			if (pwman.vault[i].id == itemId) {
				item = pwman.vault[i]
				break
			}
		}
		item.item = {
			username,
			password,
			website: websites,
			notes
		}
		await pwman.helpers.syncVaultToLocalStorage()
		await pwman.showScreen('main-menu')
	})
	document.querySelector('#edit-item form .back').addEventListener('click', async (e) => {
		e.preventDefault()
		await pwman.showScreen('main-menu')
	})
}

pwman.screens['edit-item'].show = async (item) => {
	document.querySelector('#edit-item .website').value = item.item.website.join(', ')
	document.querySelector('#edit-item .username').value = item.item.username
	document.querySelector('#edit-item .password').value = item.item.password
	const notesEl = document.querySelector('#edit-item .notes')
	const notes = item.item.notes || ''
	notesEl.value = notes
	const MIN_ROWS = 5
	notesEl.setAttribute('rows', Math.max((notes.match(/\n/g) || []).length, MIN_ROWS))
	document.querySelector('#edit-item .item-id').value = item.id
}
