pwman.screens.login = {}
pwman.screens.login.setup = () => {
	document.querySelector('#login form').addEventListener('submit', async (e) => {
		e.preventDefault()
		const password = document.querySelector('#login .password').value

		let storedInfo = await browser.storage.local.get(['vaultKey', 'masterKeySalt'])
		pwman.masterKey = await pwman.helpers.deriveMasterKey({
			password,
			vaultKey: storedInfo.vaultKey,
			salt: storedInfo.masterKeySalt
		})
		const exportedKey = await window.crypto.subtle.exportKey('jwk', pwman.masterKey)
		await browser.storage.local.set({
			masterKey: exportedKey,
		})

		storedInfo = await browser.storage.local.get(['vault', 'iv'])

		pwman.vault = JSON.parse(pwman.helpers.ab2str(await window.crypto.subtle.decrypt({
			name: 'AES-GCM',
			iv: storedInfo.iv,
		}, pwman.masterKey, storedInfo.vault)))
		await pwman.showScreen('main-menu')
	})
}

pwman.screens.login.showError = (message) => {
	const errorMessageEl = document.querySelector('#login .error')
	errorMessageEl.classList.remove('hidden')
	errorMessageEl.textContent = message
}
