(() => {
	pwman.screens['missing-vault'] = {}
	pwman.screens['missing-vault'].setup = () => {
		document.querySelector('#missing-vault button.create-vault').addEventListener('click', async (e) => {
			e.preventDefault()
			await pwman.showScreen('create-vault')
		})
		document.querySelector('#missing-vault button.import-vault-drop-box').addEventListener('click', async (e) => {
			e.preventDefault()
			await browser.runtime.sendMessage({
				action: 'dropbox-import'
			})
		})
		document.querySelector("#redirect-url").textContent = browser.identity.getRedirectURL()
	}
})()
