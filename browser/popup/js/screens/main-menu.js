(() => {
	pwman.screens['main-menu'] = {}
	pwman.screens['main-menu'].setup = async () => {
		document.querySelector('#main-menu button.logout').addEventListener('click', async (e) => {
			e.preventDefault()
			await browser.storage.local.set({
				masterKey: null
			})
			pwman.vault = null
			await pwman.showScreen('login')
		})
		document.querySelector('#main-menu button.add-item').addEventListener('click', async (e) => {
			e.preventDefault()
			await pwman.showScreen('add-item')
		})
		const searchInputEl = document.querySelector('#main-menu input.search')
		searchInputEl.addEventListener('input', pwman.helpers.debounce(100, async (e) => {
			e.preventDefault()
			let items
			try {
				const MIN_FUZZY_SEARCH_CHARS = 2
				if (searchInputEl.value && searchInputEl.value.length >= MIN_FUZZY_SEARCH_CHARS) {
					items = await pwman.helpers.searchItems({
						origin: searchInputEl.value,
						fuzzy: true
					})
				} else {
					items = await pwman.helpers.searchItemsForCurrentPage()
				}
			} catch (e) {
				pwman.log("Could not fetch items: ", e)
			}
			if (items) {
				renderItems(items)
			}
		}))
		document.querySelector('#main-menu button.generate-password').addEventListener('click', async (e) => {
			e.preventDefault()
			await pwman.showScreen('generate-password')
		})
		const dropboxConnectEl = document.querySelector('#main-menu button.dropbox-connect')
		dropboxConnectEl.addEventListener('click', async (e) => {
			e.preventDefault()
			await browser.runtime.sendMessage({
				action: 'dropbox-auth'
			})
		})
		document.querySelector("#redirect-url").textContent = browser.identity.getRedirectURL()
		const storedItems = await browser.storage.local.get(['dropboxAccessToken'])
		if (storedItems.dropboxAccessToken) {
			dropboxConnectEl.classList.add('hidden')
		}
	}

	const renderItems = (items) => {
		const currentItemsULEL = document.querySelector('#main-menu .current-items')
		currentItemsULEL.innerHTML = ''
		var itemTemplate = document.querySelector('#item-row')
		items.forEach(i => {
			const liEl = itemTemplate.content.cloneNode(true)
			liEl.querySelector(".website").textContent = i.item.website.join(', ')
			liEl.querySelector(".username").textContent = i.item.username
			liEl.querySelector("button.fill").addEventListener('click', (e) => {
				e.preventDefault()
				pwman.helpers.fillItemOnBrowserPage(i.item)
			})
			liEl.querySelector("button.edit").addEventListener('click', async (e) => {
				e.preventDefault()
				await pwman.showScreen('edit-item', i)
			})
			currentItemsULEL.appendChild(liEl)
		})
	}

	pwman.screens['main-menu'].show = async () => {
		let items
		try {
			items = await pwman.helpers.searchItemsForCurrentPage()
		} catch (e) {
			pwman.log("Could not fetch items: ", e)
		}
		if (items) {
			renderItems(items)
		}
		if (pwman.dropboxSyncFailed) {
			const syncFailedWarningEl = document.querySelector('#main-menu .sync-failed-warning')
			syncFailedWarningEl.classList.remove('hidden')
		}
		const searchInputEl = document.querySelector('#main-menu input.search')
		searchInputEl.focus()
	}
})()
