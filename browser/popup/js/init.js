/* eslint-disable no-console */

/*
 * FIXME: try/catch for http status/network errors?
 * FIXME: refactor for sane code organization
 */
window.addEventListener('DOMContentLoaded', async () => {
	const screenEls = document.querySelectorAll('.screen')

	pwman.showScreen = (id, ...rest) => {
		for (let i = 0; i < screenEls.length; i++) {
			if (screenEls[i].id == id) {
				screenEls[i].classList.remove('hidden')
			} else {
				screenEls[i].classList.add('hidden')
			}
		}
		if (pwman.screens[id].show) {
			pwman.screens[id].show(...rest)
		}
	}

	/*
	 * Set up all events
	 */
	pwman.screens.login.setup()
	pwman.screens['main-menu'].setup()
	pwman.screens['add-item'].setup()
	pwman.screens['edit-item'].setup()
	pwman.screens['generate-password'].setup()
	pwman.screens['create-vault'].setup()
	pwman.screens['missing-vault'].setup()

	// Startup
	;(async () => {

		// Note: Convient way to reset all local storage. Used for debugging
		if (false) {
			await browser.storage.local.set({
				masterKey: null,
				vault: null,
				iv: null,
				masterKeySalt: null,
				vaultKey: null
			})
		}

		const storedInfo = await browser.storage.local.get(['masterKey', 'vault', 'iv'])
		if (!storedInfo.vault || !storedInfo.iv) {
			await pwman.showScreen('missing-vault')
			return
		}
		if (!storedInfo.masterKey) {
			await pwman.showScreen('login')
			return
		}
		await pwman.helpers.loadVaultFromLocalStorage()

		const {dropboxSyncFailed} = await browser.storage.local.get(['dropboxSyncFailed'])
		if (dropboxSyncFailed) {
			pwman.dropboxSyncFailed = true
		}
		await pwman.showScreen('main-menu')
	})()
})
