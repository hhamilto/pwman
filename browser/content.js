/* eslint-disable no-console */
// TODO ^ logger maybe?
const userNameSelectors = [
	'input:not([type="radio"]):not([type="hidden"])[placeholder*="User ID" i]',
	'input:not([type="radio"]):not([type="hidden"])[id*="userid" i]',
	'input:not([type="radio"]):not([type="hidden"])[name="user_id" i]',
	'input:not([type="radio"]):not([type="hidden"])[name*="userid" i]',
	'input:not([type="radio"]):not([type="hidden"])[name="login" i]',
	'input:not([type="radio"]):not([type="hidden"])[id*="user" i]',
	'input:not([type="radio"]):not([type="hidden"])[id*="username" i]',
	'input:not([type="radio"]):not([type="hidden"])[name*="username" i]',
	'input:not([type="radio"]):not([type="hidden"])[id*="usrname" i]',
	'input:not([type="radio"]):not([type="hidden"])[placeholder*="email" i]',
	'input:not([type="radio"]):not([type="hidden"])[type="email" i]',
]

const logger = console

const passwordSelectors = ['input[type="password"]']

;(() => {
	const fillUsername = (username, options = {}) => {
		const usernameEls = []
		for (let i = 0; i < userNameSelectors.length; i++) {
			const queriedUsernameEls = [...document.querySelectorAll(userNameSelectors[i])]
			for (const usernameEl of queriedUsernameEls) {
				if (usernameEl.offsetParent == null) {
					continue
				}
				usernameEls.push(usernameEl)
			}
		}
		for (let i = 0; i < usernameEls.length; i++) {
			const usernameEl = usernameEls[i]
			usernameEl.focus()
			usernameEl.value = username
			usernameEl.click()
			const changeEvent = new Event('change', {
				bubbles: true,
				cancelable: true,
			})
			usernameEl.dispatchEvent(changeEvent)
			const inputEvent = new Event('input', {
				bubbles: true,
				cancelable: true,
			})
			usernameEl.dispatchEvent(inputEvent)
			usernameEl.blur()
		}
		// If we found any username elements, then we count as successful fill
		if (usernameEls.length) {
			return true
		}
		// Fall back to filling the input before the password selector
		if (!options.passwordEls || !options.passwordEls[0]) {
			return false
		}
		const parentFormOfPassword = options.passwordEls[0].closest('form')
		if (!parentFormOfPassword) {
			return false
		}
		const formInputs = [...parentFormOfPassword.querySelectorAll('input')]
		let usernameEl
		for (let i = 1; i < formInputs.length; i++) {
			if (formInputs[i] == options.passwordEls[0]) {
				usernameEl = formInputs[i - 1]
			}
		}
		if (!usernameEl) {
			return false
		}
		usernameEl.focus()
		usernameEl.value = username
		usernameEl.click()
		const changeEvent = new Event('change', {
			bubbles: true,
			cancelable: true,
		})
		usernameEl.dispatchEvent(changeEvent)
		const inputEvent = new Event('input', {
			bubbles: true,
			cancelable: true,
		})
		usernameEl.dispatchEvent(inputEvent)
		usernameEl.blur()
		return true
	}

	const fillPassword = (password, options = {}) => {
		let passwordEls = []
		for (let i = 0; i < passwordSelectors.length; i++) {
			const passwordElsFound = document.querySelectorAll(passwordSelectors[i])
			if (passwordElsFound.length) {
				passwordEls.push(...passwordElsFound)
				if (!options.fillAllPasswords) {
					passwordEls = [passwordEls[0]]
					break
				}
			}
		}
		for (let i = 0; i < passwordEls.length; i++) {
			const passwordEl = passwordEls[i]
			passwordEl.focus()
			passwordEl.value = password
			passwordEl.click()
			const changeEvent = new Event('change', {
				bubbles: true,
				cancelable: true,
			})
			passwordEl.dispatchEvent(changeEvent)
			const inputEvent = new Event('input', {
				bubbles: true,
				cancelable: true,
			})
			passwordEl.dispatchEvent(inputEvent)
			passwordEl.blur()
		}
		// If we found a password element, then we count as successful fill
		return {
			fillSuccesfull: !!passwordEls.length,
			passwordEls
		}
	}
	const fillItem = (item, options) => {
		let filledUsername = false
		let filledPassword = false
		if (item.username && !options?.previousFillResult?.filledUsername) {
			filledUsername = fillUsername(item.username, options)
		}
		if (item.password) {
			const result = fillPassword(item.password, options)
			filledPassword = result.fillSuccesfull
			if (item.username && !filledUsername && !options?.previousFillResult?.filledUsername) {
				filledUsername = fillUsername(item.username, {
					...options,
					passwordEls: result.passwordEls
				})
			}
		}
		return {
			filledPassword,
			filledUsername
		}
	}

	browser.runtime.onMessage.addListener(function(message, sender, sendResponse) {
		if (message.action == 'fetch url') {
			sendResponse(String(window.location))
		} else if (message.action == 'fetch username') {
			for (let i = 0; i < userNameSelectors.length; i++) {
				const els = document.querySelectorAll(userNameSelectors[i])
				for (let j = 0; j < els.length; j++) {
					if (els[j].value) {
						sendResponse(els[j].value)
						return
					}
				}
			}
		} else if (message.action == 'fetch password') {
			for (let i = 0; i < passwordSelectors.length; i++) {
				const els = document.querySelectorAll(passwordSelectors[i])
				for (let j = 0; j < els.length; j++) {
					if (els[j].value) {
						sendResponse(els[j].value)
						return
					}
				}
			}
		} else if (message.action == 'fill item') {
			fillItem(message.item, message.options)
		} else if (message.action == 'request autofill') {
			fillItem(message.item, message.options)
		} else {
			logger.log('unrecognized action', message)
		}
	})

	const requestAutoFill = async () => {
		let items
		try {
			items = await browser.runtime.sendMessage({
				action: 'request fill',
				origin: window.location.origin
			})
		} catch (e) {
			logger.log('Could not autofill:', e)
		}
		if (!items) {
			return
		}
		const fillResults = []
			for (let i = 0; i < items.length; i++) {
				fillResults[i] = fillItem(items[i].item, {
					previousFillResult: fillResults[i]
				})
				if (fillResults[i].filledPassword && fillResults[i].filledUsername) {
					// Note: If item was filled successfully, remove from array
					items.splice(i, 1)
				}
			}

	}
})()
