# PWMAN
## (PassWord MANager)

The broad strokes
- Manages passwords
- Is a firefox extension (under `browser`)
- Backend is Node.js express app (under `server`)
- Passwords are stored unencrypted in mysql (table structure in `server/sql.sql`)

# Getting started

1. Create a mysql database
2. Execute the table create sql file `server/sql.sql` against your db to setup the tables
3. Copy `server/envs/example.env` to `server/envs/dev.env` then fill it out and SOURCE the file
4. Run `node server/index.js`
5. Load the extension in to firefox by following the steps [here](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Your_first_WebExtension#installing). The extension is inside the `browser` directory
6. Curl to create your user so you can login via the extension: `curl -X POST 'http://localhost:3000/users' -H 'content-type: application/json' -d '{"username":"foo","password":"bar"}'`



Outstanding issues:
- how to backup db
- deployments
- tests


# The crypto WHY DID I NOT WRITE THIS SECTION WHILE WRITING THE CRYPTO?

# Overview
passwords/secrets are backed up to drop box. They are stored in a file named pwman-backup.json.
The contents of that file are an object with the following schema:

{
	vaultKey: Private key used to decrypt the vault. Encrypted using the masterKey + password
	vault: Bytes containing the encrypted vault. Encrypted using the AES-GCM algorithm using the `iv` and the `masterKey`
	iv: (96 bit Uint8Array) The initialization vector (used for encrypting the vault, changes every time we encrypt)
	masterKeySalt: Salt for the master key (master key is kind of like a password, so needs salt)
}


The decrypted vault is an array with the following structure:

[
	{ // an item
		username,
		password,
		website: websites,
		notes
	},
	// ...other items...
]

This object contains everything to decrypt passwords except for the master password and `vaultKey` ( 128 bits randomly generated, stored on device, do not lose)

To decrypt the data you need the secret key which is not backed up along side the data


# How vaults are generated

1/ `vaultKey` is generated 128 random hex sting
2/ user types their master password (`masterPassword`)
5/ `salt` is 128 random bits
3/ `masterKey` is derived using PBKDF2 with `vaultKey`, `masterPassword`, and `salt` as inputs.
6/ use `masterKey` to encrypt an empty vault `[]` via AES-GCM


## Diagram for encryption:
```

   masterPassword   ───┐
                       │
   vaultKey   ──────┐  │
                    │  │
   salt   ─────┐    │  │
               │    │  │
               │    ▼  ▼
               └─► PBKDF2

                      │
                      ▼
                  passwordKey
                      │
                      └──────┐
                             ▼
   unencrypted vault ───► AES-GCM
                             │
                             │
                             │
                             ▼
                     encrypted vault
```

# How vaults are accessed (decrypted)

1/ `vaultKey` + `masterPassword` + `salt` are put thru the PBKDF2 to generate the `passwordKey`
2/ `masterKey` is decrypted using `passwordKey`
3/ `masterKey` + `salt` are used to decrypt to vault using AES-GCM

tl;dr same key derivation, then decrypt the vault
